# Google Play Billing Notifications / Google Cloud Function

Deploy this function in Google Cloud and [configure](https://cloud.google.com/functions/docs/calling/pubsub) a Pub/Sub subscription for [billing notifications](https://developer.android.com/google/play/billing/realtime_developer_notifications#create_a_pubsub_subscription).
