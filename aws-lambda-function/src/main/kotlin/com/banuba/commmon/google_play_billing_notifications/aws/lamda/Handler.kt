package com.banuba.commmon.google_play_billing_notifications.aws.lamda

import by.dev.madhead.utils.lambda_proxy_integration.model.ProxyIntegrationRequest
import by.dev.madhead.utils.lambda_proxy_integration.model.ProxyIntegrationResponse
import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestHandler
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class Handler : RequestHandler<ProxyIntegrationRequest, ProxyIntegrationResponse> {
    private val logger: Logger = LogManager.getLogger(Handler::class.java)

    override fun handleRequest(input: ProxyIntegrationRequest, context: Context): ProxyIntegrationResponse = try {
        logger.debug("Got input: {}", input)

        ProxyIntegrationResponse(
                200,
                emptyMap(),
                "{}"
        )
    } catch (e: Exception) {
        when (e) {
            is JsonParseException, is JsonMappingException -> {
                logger.warn("Bad request!", e)

                ProxyIntegrationResponse(
                        400,
                        emptyMap(),
                        "{\"message\":\"${e.message ?: e.toString()}\"}"
                )
            }
            else -> {
                logger.error("Unknown error!", e)

                ProxyIntegrationResponse(
                        500,
                        emptyMap(),
                        "{\"message\":\"${e.message ?: e.toString()}\"}"
                )
            }
        }
    }
}
