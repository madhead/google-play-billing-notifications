import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer

plugins {
    id("com.github.johnrengelman.shadow").version("5.1.0")
}

dependencies {
    implementation("com.amazonaws:aws-lambda-java-core:1.2.0")
    implementation("by.dev.madhead.utils.lambda_proxy_integration_java:model:1.3.2")
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.amazonaws:aws-lambda-java-log4j2:1.1.0")
    implementation("org.apache.logging.log4j:log4j-core")
    implementation("org.koin:koin-core:2.0.1")
}

tasks {
    val shadowJar: ShadowJar by getting(ShadowJar::class) {
        transform(Log4j2PluginsCacheFileTransformer::class.java)
    }
}
