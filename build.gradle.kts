import org.jetbrains.kotlin.gradle.plugin.KotlinPlatformJvmPlugin
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm").version("1.3.41").apply(false)
}

subprojects {
    apply<JavaPlugin>()
    apply<KotlinPlatformJvmPlugin>()

    repositories {
        jcenter()
    }

    dependencies {
        val implementation by configurations
        val testImplementation by configurations
        val testRuntimeOnly by configurations

        implementation(platform("com.fasterxml.jackson:jackson-bom:2.9.9"))
        implementation(platform("org.apache.logging.log4j:log4j-bom:2.12.0"))
        implementation(platform("software.amazon.awssdk:bom:2.7.6"))
        testImplementation(platform("org.junit:junit-bom:5.5.0"))
        testRuntimeOnly(platform("org.junit:junit-bom:5.5.0"))

        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        implementation("org.apache.logging.log4j:log4j-api")
        testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
        testImplementation("org.junit.jupiter:junit-jupiter-api")
        testImplementation("org.junit.jupiter:junit-jupiter-params")
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    }

    tasks {
        withType<KotlinCompile> {
            kotlinOptions.jvmTarget = "1.8"
        }

        withType<Test> {
            useJUnitPlatform()
            testLogging {
                showStandardStreams = true
            }
        }
    }
}
